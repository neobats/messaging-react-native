import React, { useState } from "react";
import { Alert, Image, StyleSheet, View } from "react-native";
import { TouchableHighlight } from "react-native-gesture-handler";
import MessageList from "./components/MessageList";
import Status from "./components/Status";
import Toolbar from "./components/Toolbar";
import { MessageShape } from "./types";
import {
  createImageMessage,
  createLocationMessage,
  createTextMessage,
  log,
  prependMessageToList,
} from "./utils";

export default function App() {
  const defaultMessageList = [
    createImageMessage("https://unsplash.it/300/300"),
    createTextMessage("Hello"),
    createTextMessage("World"),
    createLocationMessage({
      latitude: 37.78825,
      longitude: -122.4324,
    }),
  ];

  const [messages, setMessages] = useState(defaultMessageList);
  const [fullscreenImgId, setFullscreenImgId] = useState<string | null>(null);
  const [isInputFocused, setInputFocused] = useState(false);

  const getMessages = () => messages;
  // add a new message with the current list
  // currently has a type of () => MessageShape[], because of no union like textFn | locationFn
  const [
    addTextMessage,
    addImageMessage,
    addLocationMessage,
  ] = prependMessageToList(getMessages);

  const dismissFullscreenImage = () => {
    console.log("dismissing Image", fullscreenImgId);
    setFullscreenImgId(null);
  };
  const logRender = (render: { (): JSX.Element | null; (): any }) => {
    const result = log(render);
    console.log("logRender fullscreenImgId", fullscreenImgId);
    return result;
  };
  const filterMessage = (id: string) => (msg: MessageShape) => msg.id !== id;

  const handlePressMessage = ({ id, type }: MessageShape) => {
    switch (type) {
      case "text":
        Alert.alert(
          "Delete message?",
          "Are you sure you want to permanently delete this message?",
          [
            {
              text: "Cancel",
              style: "cancel",
            },
            {
              text: "Delete",
              style: "destructive",
              onPress: () => setMessages(messages.filter(filterMessage(id))),
            },
          ]
        );
        break;
      case "image":
        return setFullscreenImgId(id);
      default:
        break;
    }
  };

  const handlePressToolbarLocation = () => {};

  const handlePressToolbarCamera = () => {};

  const handleSubmit = (text: string) => {
    setMessages([createTextMessage(text), ...messages]);
  };

  const renderMessageList = () => (
    <View style={styles.content}>
      <MessageList messages={messages} onPressMessage={handlePressMessage} />
    </View>
  );
  const renderInputMethodEditor = () => (
    <View style={styles.inputMethodEditor}></View>
  );
  const renderToolbar = () => (
    <View style={styles.toolbar}>
      <Toolbar />
    </View>
  );
  const renderFullscreenImage = () => {
    console.log("renderFullscreenImage imgId:", fullscreenImgId);
    if (!fullscreenImgId) return null;

    const image = messages.find((message) => message.id === fullscreenImgId);
    console.log("image", image);
    if (!image) return null;

    const { uri } = image;

    return (
      <TouchableHighlight
        style={styles.fullscreenOverlay}
        onPress={dismissFullscreenImage}
      >
        <Image style={styles.fullscreenImage} source={{ uri }} />
      </TouchableHighlight>
    );
  };

  return (
    <View style={styles.container}>
      <Status />
      {renderMessageList()}
      {renderToolbar()}
      {renderInputMethodEditor()}
      {logRender(renderFullscreenImage)}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  content: {
    flex: 1,
    backgroundColor: "white",
  },
  inputMethodEditor: {
    flex: 1,
    backgroundColor: "white",
  },
  toolbar: {
    borderTopWidth: 1,
    borderTopColor: "rgba(0,0,0,0.4)",
    backgroundColor: "white",
  },
  fullscreenOverlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "black",
    zIndex: 2,
  },
  fullscreenImage: {
    flex: 1,
    resizeMode: "contain",
  },
});
