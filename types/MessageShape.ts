export type MessageShapeType = "text" | "image" | "location";
export type Coordinate = {
  latitude: number;
  longitude: number;
};

export interface MessageShape {
  id: string;
  type?: MessageShapeType;
  text?: string;
  uri?: string;
  coordinate?: Coordinate;
}
