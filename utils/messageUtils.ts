import * as _ from "ramda";
import "react-native-get-random-values";
import { v4 as uuid } from "uuid";
import { Coordinate, MessageShape, MessageShapeType } from "../types";

type IncompleteMessage = Partial<MessageShape>;

const addText = (text: string): IncompleteMessage => ({ text });
const addUri = (uri: string): IncompleteMessage => ({ uri });
const addLocation = (coordinate: Coordinate): IncompleteMessage => ({
  coordinate,
});

const checkMessageType = (type: MessageShapeType) => {
  switch (type) {
    case "text":
      return addText;
    case "image":
      return addUri;
    case "location":
      return addLocation;
  }
};

const messageWithType = (idFactory: () => string) => (
  type: MessageShapeType
): MessageShape => ({ id: idFactory(), type });
const createEmptyMessage = messageWithType(uuid);

type locationFn = (co: Coordinate) => MessageShape;
type textFn = (text: string) => MessageShape;

type messageCreator = <T extends "text" | "image" | "location">(
  t: T
) => locationFn | textFn;
const createMessage: messageCreator = (t) => {
  if (t === "location") {
    return ((input: Coordinate): MessageShape => ({
      ...createEmptyMessage(t),
      ...checkMessageType(t)(input as string & Coordinate),
    })) as locationFn;
  }
  return ((input: string): MessageShape => ({
    ...createEmptyMessage(t),
    ...checkMessageType(t)(input as string & Coordinate),
  })) as textFn;
};

type messageCreatorTuple = [textFn, textFn, locationFn];
const createSingleMessageTuple = _.map(createMessage)([
  "text",
  "image",
  "location",
]) as messageCreatorTuple;

export const [
  createTextMessage,
  createImageMessage,
  createLocationMessage,
] = createSingleMessageTuple;

export const prependMessageToList = ((fns: messageCreatorTuple) => (
  msgGetter: () => MessageShape[]
) => {
  const curriedAppend = _.curry(_.flip(_.prepend))(msgGetter()) as (
    message: MessageShape
  ) => MessageShape[];
  // doesn't give me argument types. Infers a return type of () => MessageShape[]
  return _.map((fn) => _.compose(curriedAppend, fn as any))(fns);
  // throws an error with the union, but works with one of the two types.
  // return _.map((fn) => _.compose(curriedAppend, fn as textFn | locationFn))(fns);
})(createSingleMessageTuple);

// more traditional version of createTextMessage
// const addMessageText = (message: MessageShape) => (text: string) => ({
//   ...message,
//   text,
// });
