export const log = (fn: () => any) => {
  const result = fn();
  console.log(result);
  return result;
};
