import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import MapView, { LatLng, Marker, Region } from "react-native-maps";
import { MessageShape } from "../types";

type Props = {
  messages: MessageShape[];
  onPressMessage: (item: MessageShape) => void;
};

export default function MessageShapeList({ messages, onPressMessage }: Props) {
  const keyExtractor = (item: { id: { toString: () => any } }) =>
    item.id.toString();

  const renderMessageBody: (
    body: Pick<MessageShape, "type" | "text" | "uri" | "coordinate">
  ) => JSX.Element | null = ({ type, text, uri, coordinate }) => {
    switch (type) {
      case "text":
        return (
          <View style={styles.messageBubble}>
            <Text style={styles.text}>{text}</Text>
          </View>
        );
      case "image":
        return <Image style={styles.image} source={{ uri }} />;
      case "location":
        return (
          <MapView
            style={styles.map}
            initialRegion={
              {
                ...coordinate,
                latitudeDelta: 0.08,
                longitudeDelta: 0.04,
              } as Region
            }
          >
            <Marker coordinate={{ ...coordinate } as LatLng} />
          </MapView>
        );
      default:
        return null;
    }
  };

  const renderMessageItem = ({ item }: { item: MessageShape }) => (
    <View key={item.id} style={styles.messageRow}>
      <TouchableOpacity onPress={() => onPressMessage(item)}>
        {renderMessageBody(item)}
      </TouchableOpacity>
    </View>
  );
  return (
    <FlatList
      style={styles.container}
      inverted
      data={messages}
      renderItem={renderMessageItem}
      keyExtractor={keyExtractor}
      keyboardShouldPersistTaps="handled"
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    overflow: "visible",
  },
  messageRow: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginBottom: 4,
    marginRight: 10,
    marginLeft: 60,
  },
  messageBubble: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: "rgb(16,135,255)",
    borderRadius: 20,
  },
  text: {
    fontSize: 18,
    color: "white",
  },
  image: {
    width: 150,
    height: 150,
    borderRadius: 10,
  },
  map: {
    width: 250,
    height: 250,
    borderRadius: 10,
  },
});
