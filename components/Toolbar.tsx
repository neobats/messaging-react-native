import React, { useEffect, useMemo, useRef, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";

type Props = {
  isFocused?: boolean;
  onChangeFocus?: (val: boolean) => void;
  onSubmit?: (text: string) => void;
  onPressCamera?: () => void;
  onPressLocation?: () => void;
};

type ButtonProps = { title: string; onPress: () => void };
const ToolbarButton = ({ title, onPress }: ButtonProps) => (
  <TouchableOpacity onPress={onPress}>
    <Text style={styles.button}>{title}</Text>
  </TouchableOpacity>
);

export default function Toolbar({
  isFocused = false,
  onChangeFocus = () => {},
  onPressCamera = () => {},
  onSubmit = () => {},
  onPressLocation = () => {},
}: Props) {
  const [text, setText] = useState("");
  const input = useRef<null | { focus: () => void; blur: () => void }>(null);
  const prevIsFocused = useRef(false);

  useEffect(() => {
    prevIsFocused.current = isFocused;
  }, []);

  useMemo(() => {
    if (prevIsFocused.current !== isFocused) {
      if (isFocused) {
        input.current?.focus();
      }
    }
  }, [isFocused, prevIsFocused.current, input.current]);

  const handleFocus = () => onChangeFocus(true);
  const handleBlur = () => onChangeFocus(false);
  const handleSubmitEditing = () => {
    if (!text) return;

    onSubmit(text);
    setText("");
  };

  return (
    <View style={styles.toolbar}>
      <ToolbarButton title={"📸"} onPress={onPressCamera} />
      <ToolbarButton title={"📍"} onPress={onPressLocation} />
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          ref={input as any}
          underlineColorAndroid="transparent"
          placeholder="Type Something!"
          blurOnSubmit={false}
          value={text}
          onChangeText={setText}
          onSubmitEditing={handleSubmitEditing}
          onFocus={handleFocus}
          onBlur={handleBlur}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  toolbar: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 10,
    paddingLeft: 16,
    backgroundColor: "white",
  },
  button: {
    top: -2,
    marginRight: 12,
    fontSize: 20,
    color: "grey",
  },
  inputContainer: {
    flex: 1,
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "rgba(0,0,0,0.04)",
    borderRadius: 16,
    paddingVertical: 4,
    paddingHorizontal: 12,
    backgroundColor: "rgba(0,0,0,0.02)",
  },
  input: {
    flex: 1,
    fontSize: 18,
  },
});
